## Image Downloader

### Content

[1. How to run program](#how-to-run-this-programm)

[2. Description](#description)

[3. Screenshots](#screenshots)

[4. Keywords](#keywords)

### How to run this program:

To run this program you will need to clone the project _[git clone https://gitlab.com/andrius.laurusevicius_sda/forecast.git]_. Open this project in your IDE. In ImageSaver class (_src/main/java/com/andrius/imagedownloader/dbservice/ImageSaver_) you need to change the **FOLDER_PATH** value to your own folder path, where you want to save images and run the Main (Main.java) class.

### Description

It's a simple web scrapper, which fetches image links from Shutterstock.com and downloads watermarked images. Main features of the app:
- photo search is made according to user input;
- a path builder creates a URL with parameters(additional search options), according to user selection;
- HTTP response filter, to get valid image links;
- image saving (up to 100 images) to the selected folder;

### Screenshots

![alt text](https://i.ibb.co/F5pwHjd/image-saver-1.png)

### Keywords:

HttpRequest/HttpResponse, Enums, Collections API, NIO, Maven project, Exceptions.