package com.andrius.imagedownloader;

import com.andrius.imagedownloader.dbservice.ImageSaver;
import com.andrius.imagedownloader.httpservice.PathBuilder;
import com.andrius.imagedownloader.httpservice.RequestService;
import com.andrius.imagedownloader.stringservice.LinkSeparator;
import com.andrius.imagedownloader.utils.Messages;

import java.util.List;

/**
 * Code created by Andrius on 2020-09-09
 */
public class AppController {

    public void run() {
        Messages.printWelcomeMessage();
        String urlPath = new PathBuilder().getPath();
        Messages.printUrl(urlPath);
        String response = new RequestService().getHttpResponse(urlPath);
        LinkSeparator separator = new LinkSeparator();
        String[] textArray = separator.getStringArrayFromHttpResponse(response);
        List<String> links = separator.getLinksFromStringArray(textArray);
        Messages.printFoundImageCount(links);
        new ImageSaver().saveImagesFromLinksList(links);
    }
}
