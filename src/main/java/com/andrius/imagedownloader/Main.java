package com.andrius.imagedownloader;

import com.andrius.imagedownloader.dbservice.ImageSaver;
import com.andrius.imagedownloader.httpservice.PathBuilder;
import com.andrius.imagedownloader.httpservice.RequestService;
import com.andrius.imagedownloader.stringservice.LinkSeparator;
import com.andrius.imagedownloader.utils.Messages;

import java.util.List;

/**
 * Code created by Andrius on 2020-08-28
 */
public class Main {
    public static void main(String[] args) {

        new AppController().run();
    }
}
