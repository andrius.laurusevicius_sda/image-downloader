package com.andrius.imagedownloader.utils;

import java.util.Scanner;

/**
 * Code created by Andrius on 2020-08-27
 */
public class UserInput {

    private static Scanner scanner = new Scanner(System.in);

    private static String getUserStringInput() {
        return scanner.nextLine();
    }

    public static String getSearchTerm() {
        return getUserStringInput().trim().replace(" ", "+");
    }

    public static int getNumberInput() {
        int number;
        try {
            number = Integer.parseInt(scanner.next());
        } catch (NumberFormatException e) {
            Messages.printIntegerParsingErrorMessage();
            return getNumberInput();
        }
        return number;
    }
}
