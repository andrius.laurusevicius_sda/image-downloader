package com.andrius.imagedownloader.utils;

import java.util.List;

/**
 * Code created by Andrius on 2020-08-28
 */
public class Messages {

    public static void printConnectionErrorMessage() {
        System.err.print("Connection error. Program will close.\n");
    }

    public static void printIntegerParsingErrorMessage() {
        System.err.print("Only numbers allowed. Enter again!\n");
    }

    public static void printSavedImageMessage(int number) {
        System.out.printf("Image %d saved...%n", number);
    }

    public static void printFoundImageCount(List<String> list) {
        int count = list.size();
        System.out.printf("We found %s images%n", count);
    }

    public static void printMessage(String message) {
        System.out.printf("%s%n", message);
    }

    public static void printWelcomeMessage() {
        String separator = "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=";
        System.out.printf("%s%nImage Downloader from ShutterStock.com%n%s%n", separator, separator);
    }

    public static void printUrl(String url) {
        String separator = "---------------------------------------------------" +
                "-----------------------------------------------------------------";
        System.out.printf("%s%nRequest made to %s%n%s%n", separator, url, separator);
    }
}
