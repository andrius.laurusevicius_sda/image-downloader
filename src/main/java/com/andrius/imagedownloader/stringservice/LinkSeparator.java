package com.andrius.imagedownloader.stringservice;

import com.andrius.imagedownloader.utils.Messages;

import java.util.ArrayList;
import java.util.List;

/**
 * Code created by Andrius on 2020-08-27
 */
public class LinkSeparator {

    public String[] getStringArrayFromHttpResponse(String response) {
        String stringBeginning = response.substring(response.indexOf(StringFilter.STRING_BEGINNING.getName()));
        return stringBeginning.substring(0, stringBeginning.indexOf(StringFilter.STRING_END.getName())).split(StringFilter.STRING_SPACE.getName());
    }

    public List<String> getLinksFromStringArray(String[] array) {
        List<String> links = new ArrayList<>();
        for (String line : array
        ) {
            if (line.contains(StringFilter.LINK_BEGINNING.getName()) && line.contains(StringFilter.LINK_END.getName())) {
                String firstFilter = line.substring(line.indexOf(StringFilter.LINK_BEGINNING.getName()) + StringFilter.LINK_BEGINNING.getName().length());
                String lastFilter = firstFilter.substring(0, firstFilter.indexOf(StringFilter.LINK_END.getName()) + StringFilter.LINK_END.getName().length()).replace(StringFilter.SOLIDUS.getName(), StringFilter.SLASH.getName());
                links.add(lastFilter);
            }
        }
        return links;
    }
}