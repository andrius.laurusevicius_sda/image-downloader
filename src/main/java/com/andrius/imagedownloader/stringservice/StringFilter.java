package com.andrius.imagedownloader.stringservice;

/**
 * Code created by Andrius on 2020-08-28
 */
public enum StringFilter {
    STRING_BEGINNING("window.__INITIAL_STATE__="),
    STRING_END("window.__APP_CONFIG__="),
    STRING_SPACE(" "),
    LINK_BEGINNING("\"1500w\\\":{\\\"src\\\":\\\""),
    LINK_END(".jpg"),
    SOLIDUS("\\\\u002F"),
    SLASH("/");

    private final String name;

    StringFilter(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
