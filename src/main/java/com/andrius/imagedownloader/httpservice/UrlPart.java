package com.andrius.imagedownloader.httpservice;

/**
 * Code created by Andrius on 2020-08-28
 */
public enum UrlPart {
    PATH_BEGINNING("https://www.shutterstock.com/search/"),
    SLUG_PHOTO_ORIENTATION("&orientation="),
    SLUG_COLOR_SELECTION("&color="),
    SLUG_ORDER_BY("&sort=newest"),
    SLUG_IMAGE_TYPE("?image_type=photo");

    private final String name;

    UrlPart(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
