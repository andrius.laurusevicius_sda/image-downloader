package com.andrius.imagedownloader.httpservice;

import com.andrius.imagedownloader.utils.Messages;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

/**
 * Code created by Andrius on 2020-08-27
 */
public class RequestService {

    public String getHttpResponse(String path) {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(path))
                .build();

        HttpResponse<String> response = null;
        try {
            response = client.send(request,
                    HttpResponse.BodyHandlers.ofString());
        } catch (IOException | InterruptedException e) {
            Messages.printConnectionErrorMessage();
        }
        return response.body();
    }
}
