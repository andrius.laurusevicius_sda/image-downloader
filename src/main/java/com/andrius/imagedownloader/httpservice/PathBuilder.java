package com.andrius.imagedownloader.httpservice;

import com.andrius.imagedownloader.utils.UserInput;
import com.andrius.imagedownloader.utils.Messages;

/**
 * Code created by Andrius on 2020-08-27
 */
public class PathBuilder {

    public String getPath() {
        Messages.printMessage("\nEnter search keyword or phrase:");
        String url = UrlPart.PATH_BEGINNING.getName();
        String keywords = UserInput.getSearchTerm();
        String type = UrlPart.SLUG_IMAGE_TYPE.getName();
        String features = concatenateSelectedFeatures();
        return String.format("%s%s%s%s", url, keywords,
                type, features);
    }

    private String getPhotoOrientationValue() {
        int number = UserInput.getNumberInput();
        switch (number) {
            case 1:
                return UrlPart.SLUG_PHOTO_ORIENTATION.getName() + "horizontal";
            case 2:
                return UrlPart.SLUG_PHOTO_ORIENTATION.getName() + "vertical";
            default:
                return "";
        }
    }

    private String getColorValue() {
        int number = UserInput.getNumberInput();
        switch (number) {
            case 1:
                return UrlPart.SLUG_COLOR_SELECTION.getName() + "bw";
            case 2:
                return UrlPart.SLUG_COLOR_SELECTION.getName() + "green";
            case 3:
                return UrlPart.SLUG_COLOR_SELECTION.getName() + "blue";
            default:
                return "";
        }
    }

    private String getOderValue() {
        int number = UserInput.getNumberInput();
        if (number == 1) {
            return UrlPart.SLUG_ORDER_BY.getName();
        }
        return "";
    }

    private String concatenateSelectedFeatures() {
        Messages.printMessage("Select photo color: [1] black/white, [2] green, [3] blue. " +
                "\nEnter number or press 0 to continue without selection:");
        String color = getColorValue();
        Messages.printMessage("Select photo orientation: [1] horizontal, [2] vertical. " +
                "\nEnter number or press 0 to continue without selection:");
        String orientation = getPhotoOrientationValue();
        Messages.printMessage("Select photo order: [1] newest or press 0 to continue without selection:");
        String order = getOderValue();
        return orientation + color + order;
    }
}
