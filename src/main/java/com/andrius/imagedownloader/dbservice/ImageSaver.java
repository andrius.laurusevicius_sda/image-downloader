package com.andrius.imagedownloader.dbservice;

import com.andrius.imagedownloader.utils.Messages;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;

/**
 * Code created by Andrius on 2020-08-27
 */
public class ImageSaver {

    private static final String FOLDER_PATH = "C:\\Users\\Simona\\Pictures\\javasavedimages\\";
    private static final int IMAGE_NAME_LENGTH = 10;

    private void save(String url, String fileName, int number) {
        try (InputStream in = new URL(url).openStream()) {
            Files.copy(in, Paths.get(FOLDER_PATH + fileName), StandardCopyOption.REPLACE_EXISTING);
            Messages.printSavedImageMessage(number);
        } catch (IOException e) {
            System.err.println("Error while saving image...");
        }
    }

    public void saveImagesFromLinksList(List<String> links) {
        int counter = 1;
        for (String link : links
        ) {
            String fileName = link.substring(link.length() - IMAGE_NAME_LENGTH);
            save(link, fileName, counter);
            counter++;
        }
    }
}
